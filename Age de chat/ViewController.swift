//
//  ViewController.swift
//  Age de chat
//
//  Created by Amelie on 26/04/2019.
//  Copyright © 2019 Amelie. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    
    @IBOutlet weak var ageTextField: UITextField!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func calculerAction(_ sender: Any) {
        // Code quand bouton appuyé
        
        //Ranger le clavier
        view.endEditing(true)
        
        //Vérifier que l'utilisateur a entré du texte
        //si le texte de ageTextfield est rempli (n'est pas égal à null)
        if ageTextField.text != nil{
            //crée une constante avec le text ; le ! signifie que l'on est sur d'avoir fait la vérif
            let texte = ageTextField.text!
            //on transforme le texte en nombre entier
            if let nombreEntier = Int(texte){
                //on calcule l'age de chat en multipliant le nombre entier par 7 et on l'assigne dans la constante ageDeChat
                let ageDeChat = nombreEntier * 7
                //on affiche le resultat dans le label
                resultLabel.text = "Votre âge de chat est de : \(ageDeChat) ans."
            }
        }
    }
    
    @IBAction func soundAction(_ sender: Any) {
        // Faire parler notre app
        //si on arrive à récupérer du texte dans le résult label, alors :
        if let texte = resultLabel.text {
            //on synthétise une voix
            let speech = AVSpeechSynthesizer()
            //on initialise l'énonciation et on lui assigne le texte du resultlabel
            let utterance = AVSpeechUtterance(string: texte)
            //rate = vitesse d'énonciation de la voix : plus c'est proche de 0 plus c'est lent, plus cest proche de 1 c'est rapide
            utterance.rate = 0.5
            //on choisit la langue
            utterance.voice = AVSpeechSynthesisVoice(language: "fr_FR")
            speech.speak(utterance)
        }
    }
}

